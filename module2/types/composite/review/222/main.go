package main

import "fmt"

func mesta(a, b *int) {
	*a, *b = *b, *a

}

func main() {
	a := 5
	b := 8
	fmt.Println(a, b)
	mesta(&a, &b)
	fmt.Println(a, b)

}
