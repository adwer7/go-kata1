package main

import (
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// User создай структуру User.
type User struct {
	Name   string
	Age    int
	Income int
}

// generateAge сгенерируй возраст от 18 до 70 лет.
func generateAge() int {
	// используй rand.Intn()
	age := rand.Intn(52) + 18

	return age
}

// generateIncome сгенерируй доход от 0 до 500000.
func generateIncome() int {
	// используй rand.Intn()
	income := rand.Intn(500000)
	return income
}

// generateFullName сгенерируй полное имя. например "John Doe".

func generateFullName() string {
	// создай слайс с именами и слайс с фамилиями.
	// используй rand.Intn() для выбора случайного имени и фамилии.
	firstname := []string{"John", "Bill", "Max", "Leo", "Den", "Jack", "Annet", "Mary", "Julia"}

	lastname := []string{"Jonson", "Baffet", "Dikaprio", "Messi"}
	name := firstname[rand.Intn(len(firstname)-1)] + " " + lastname[rand.Intn(len(lastname)-1)]

	return name
}

func main() {
	// Сгенерируй 1000 пользователей и заполни ими слайс users.
	kolvo := 1000
	users := make([]User, kolvo)
	for i := 0; i < kolvo; i++ {
		users[i] = User{
			Name:   generateFullName(),
			Age:    generateAge(),
			Income: generateIncome(),
		}

	}
	fmt.Println(users)
	// Выведи средний возраст пользователей.
	n := 0
	dohod := 0
	for i := len(users) - 1; i >= 0; i-- {
		n = n + users[i].Age
	}
	fmt.Println("Средний возраст: ", n/kolvo)
	// Выведи средний доход пользователей.
	for i := len(users) - 1; i >= 0; i-- {
		dohod = dohod + users[i].Income
	}
	fmt.Println("Средний доход: ", dohod/kolvo)
	// Выведи количество пользователей, чей доход превышает средний доход.
	sal := 0
	for i := len(users) - 1; i >= 0; i-- {
		if users[i].Income > dohod/kolvo {
			sal++
		}

	}
	fmt.Println("У ", sal, " пользователей доход больше среднего")
}
