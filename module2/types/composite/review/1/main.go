package main

import (
	"fmt"
	"math/rand"
	"sort"
	"unicode/utf8"
)

type Employee struct {
	Name       string
	Age        int
	Salary     float64
	Department string
}

type Sortable interface {
	Len() int
	Less(i, j int) bool
	Swap(i, j int)
}

type ByAge []Employee

func (a ByAge) Len() int           { return len(a) }
func (a ByAge) Less(i, j int) bool { return a[i].Age < a[j].Age }
func (a ByAge) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

type BySalary []Employee

func (a BySalary) Len() int           { return len(a) }
func (a BySalary) Less(i, j int) bool { return a[i].Salary < a[j].Salary }
func (a BySalary) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

type ByDepartment []Employee

func (a ByDepartment) Len() int { return len(a) }
func (a ByDepartment) Less(i, j int) bool {
	iRune, _ := utf8.DecodeRuneInString((a[i].Department))
	jRune, _ := utf8.DecodeRuneInString((a[j].Department))
	return int32(iRune) < int32(jRune)
}
func (a ByDepartment) Swap(i, j int) { a[i], a[j] = a[j], a[i] }

func main() {
	employees := generateRandomEmployees(10)

	for {
		printMenu()
		var choice int
		fmt.Scan(&choice)

		switch choice {
		case 1:
			printEmployees(employees)
		case 2:
			sort.Sort(ByAge(employees))
			printEmployees(employees)
		case 3:
			sort.Sort(BySalary(employees))
			printEmployees(employees)
		case 4:
			sort.Sort(ByDepartment(employees))
			printEmployees(employees)
		case 5:
			fmt.Println("Goodbye!")
			return
		default:
			fmt.Println("Invalid choice!")
		}
	}
}

func generateRandomEmployees(n int) []Employee {
	// сгенерировать рандомных работников
	firstname := []string{"Den", "Annet", "Bred", "Sara", "Julia", "Michael", "Leo", "Daniel",
		"Alex", "John"}
	lastname := []string{"Jonson", "Griffin", "Simpson", "Peterson", "Vardi", "Robbins",
		"Charlin", "Bredly", "Mahn", "Giterburg"}
	dep := []string{"HR", "IT", "Secure", "PR", "Headquarters"}
	var randomemp []Employee
	for i := 0; i < 10; i++ {
		temp := Employee{
			Name:       firstname[rand.Intn(len(firstname)-1)] + " " + lastname[rand.Intn(len(lastname)-1)],
			Age:        rand.Intn(50) + 18,
			Salary:     float64(rand.Intn(100000)),
			Department: dep[rand.Intn(len(dep)-1)],
		}
		randomemp = append(randomemp, temp)
	}
	return randomemp
}

func printMenu() {
	fmt.Println("Please choose an option:")
	fmt.Println("1. Print all employees")
	fmt.Println("2. Sort employees by age")
	fmt.Println("3. Sort employees by salary")
	fmt.Println("4. Sort employees by department")
	fmt.Println("5. Exit")
}

func printEmployees(employees []Employee) {
	// вывести работников
	for i := range employees {
		fmt.Println("Name: ", employees[i].Name)
		fmt.Println("Age: ", employees[i].Age)
		fmt.Println("Salary: ", employees[i].Salary)
		fmt.Println("Department: ", employees[i].Department)
		fmt.Println("=====================================")
	}
}
