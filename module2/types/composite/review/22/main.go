package main

import "fmt"

func vhod(s []string) []string {
	result := make(map[string]int)
	for i := range s {
		result[s[i]]++
	}
	keys := []string{}
	for key := range result {
		keys = append(keys, key)
	}
	return keys

}

func main() {
	s := []string{"q", "w", "e", "q", "e", "w", "w", "t", "y", "g", "d", "w", "q", "q", "w", "w", "e", "e", "e", "r"}
	vhod(s)
	fmt.Println(s)
}
