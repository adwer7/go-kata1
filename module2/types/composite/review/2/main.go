package main

import "fmt"

func maxnumber(s []int) {
	max := s[0]
	for i := range s {
		if s[i] > max {
			max = s[i]
		}
	}
	fmt.Println("Max Number is: ", max)
}

func main() {
	s := []int{1, 5, 6, 8, 225, 10, -58, 45, -89, -23, 78}
	maxnumber(s)
}
