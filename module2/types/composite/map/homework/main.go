// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/sashabaranov/go-openai",
			Stars: 3300,
		},
		{
			Name:  "https://github.com/Dreamacro/clash",
			Stars: 39300,
		},
		{
			Name:  "https://github.com/eryajf/chatgpt-dingtalk",
			Stars: 1300,
		},
		{
			Name:  "https://github.com/869413421/chatgpt-web",
			Stars: 2300,
		},
		{
			Name:  "https://github.com/ccfos/nightingale",
			Stars: 6200,
		},
		{
			Name:  "https://github.com/fatedier/frp",
			Stars: 65800,
		},
		{
			Name:  "https://github.com/smartcontractkit/chainlink",
			Stars: 4400,
		},
		{
			Name:  "https://github.com/XTLS/REALITY",
			Stars: 1400,
		},
		{
			Name:  "https://github.com/cloudflare/cloudflared",
			Stars: 5400,
		},
		{
			Name:  "https://github.com/CryoByte33/steam-deck-utilities",
			Stars: 1700,
		},
		{
			Name:  "https://github.com/go-admin-team/go-admin",
			Stars: 8800,
		},
		{
			Name:  "https://github.com/KeXueShangWangkexue",
			Stars: 15200,
		},
	}

	// в цикле запишите в map
	projectmap := map[string]Project{}
	for i := range projects {
		projectmap[projects[i].Name] = projects[i]

	}

	// в цикле пройдитесь по мапе и выведите значения в консоль
	for i := range projectmap {
		fmt.Println("Ключ ", i)
		fmt.Println("Значение ", projectmap[i])
		fmt.Println("=============================== ")
	}
}
