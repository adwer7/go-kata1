package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")

	typeUint()
}
func typeUint() {
	fmt.Println("===START TYPE UinT===")
	var numberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8:", numberUint8, "size is:", unsafe.Sizeof(numberUint8), "bytes")
	numberUint8 = (1 << 8) - 1
	fmt.Println(numberUint8)
	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println("left shift uint8:", numberUint16, "size is:", unsafe.Sizeof(numberUint16), "bytes")
	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println("left shift uint8:", numberUint32, "size is:", unsafe.Sizeof(numberUint32), "bytes")
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("left shift uint8:", numberUint64, "size is:", unsafe.Sizeof(numberUint64), "bytes")
	fmt.Println("===END TYPE UinT===")
}
