package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var uintNumber uint16 = 1 << 15
	var from = int16(uintNumber)
	uintNumber--
	var to = int16(uintNumber)
	fmt.Println(from, to)
	typeInt()
}

func typeInt() {
	fmt.Println("===START TYPE UinT===")
	var uintNumber uint8 = 1 << 7
	var min = int8(uintNumber)
	uintNumber--
	var max = int8(uintNumber)
	fmt.Println(min, max)
	fmt.Println("int8 min value", min, "int8 max value", max, "size:", unsafe.Sizeof(min), "bypes")
	var uintNumber16 uint16 = 1 << 15
	var min16 = int16(uintNumber16)
	uintNumber16--
	var max16 = int16(uintNumber16)
	fmt.Println(min16, max16)
	fmt.Println("int16 min value", min16, "int16 max value", max16, "size:", unsafe.Sizeof(min16), "bypes")
	var uintNumber32 uint32 = 1 << 31
	var min32 = int32(uintNumber32)
	uintNumber32--
	var max32 = int32(uintNumber32)
	fmt.Println(min32, max32)
	fmt.Println("int32 min value", min32, "int32 max value", max32, "size:", unsafe.Sizeof(min32), "bypes")
	var uintNumber64 uint64 = 1 << 63
	var min64 = int64(uintNumber64)
	uintNumber64--
	var max64 = int64(uintNumber64)
	fmt.Println(min64, max64)
	fmt.Println("int64 min value", min64, "int64 max value", max64, "size:", unsafe.Sizeof(min64), "bypes")

	fmt.Println("===END TYPE UinT===")

}
